var soudyTexty = {
	'Ústavní soud': 'foo',
	'Nejvyšší soud': 'foo',
	'Vrchní soud v Praze': 'foo',
	'Městský soud v Praze': 'foo',
	'Krajský soud v Praze': 'foo?',
	'Obvodní soud pro Prahu 1': 'foo',
	'Obvodní soud pro Prahu 2': 'foo',
	'Obvodní soud pro Prahu 4': 'foo',
	'Obvodní soud pro Prahu 6': 'foo',
	'Obvodní soud pro Prahu 7': 'foo'
};

var soudyPole = ['Ústavní soud', 'Nejvyšší soud', 'Vrchní soud v Praze', 'Městský soud v Praze',
'Krajský soud v Praze',
'Obvodní soud pro Prahu 1',
'Obvodní soud pro Prahu 2',
'Obvodní soud pro Prahu 4',
'Obvodní soud pro Prahu 6',
'Obvodní soud pro Prahu 7'
];

function status(stat) {
	var out;
	switch (stat) {
		case 'vyhrál':
			out = 'win';
			break;
		case 'prohrál':
			out = 'lost';
			break;
		default:
			out = 'gray';
	};
	return out;
};

function timeFrame(casted, decided) {
	var out = '<em>';
	if (casted != 'none') {
		casted = casted.split(' ')[0].split('-');
		var cd = new Date(parseInt(casted[0]), parseInt(casted[1]) - 1, parseInt(casted[2]));
		out += ', podáno ' + cd.getDate() + '. ' + (cd.getMonth() + 1) + '. ' + cd.getFullYear();
	}

	if (decided != 'none') {
		decided = decided.split(' ')[0].split('-');
		var dd = new Date(parseInt(decided[0]), parseInt(decided[1]) - 1, parseInt(decided[2]));
		out += ', rozhodnuto ' + dd.getDate() + '. ' + (dd.getMonth() + 1) + '. ' + dd.getFullYear();
	}

	var span = (dd - cd) / (1000 * 60 * 60 * 24);
	if (span == span) {
		out += ' (' + Math.round(span) + ' dní)';
	}

	out += '</em>';
	return out;
};

$.getJSON('https://interaktivni.rozhlas.cz/data/rath-soudy/data.json', function (dt) {
	var data = {};

	$.each(soudyPole, function(i, el) {
		data[el] = dt[el];
	});


	var out = '<div class="panel-group">';
	$.each(soudyPole, function(i, soud) {

		var pripady = data[soud];
		
		out += '<div class="panel panel-default">' + '<div class="panel-heading">' + '<h2 class="panel-title">' + ' <a data-toggle="collapse"  href="#collapse' + i + '">' + soud + '</a>' + ' <span class="score">(<span class="win">vyhrál ' + pripady.filter(function (e) {
			return e.stav == 'vyhrál';
		}).length //prohral
		+ '</span>, <span class="lost">prohrál ' + pripady.filter(function (e) {
			return e.stav == 'prohrál';
		}).length //prohral
		+ '</span>, <span class="gray">v běhu ' + pripady.filter(function (e) {
			return e.stav != 'vyhrál' && e.stav != 'prohrál';
		}).length //prohral
		+ '</span>, celkem <span class="black">' + pripady.length + '</span>)</span></h2></div>'
		//+ '<p class="details">'
		//+ soudyTexty[soud]
		//+ '</p>'
		+ '<div id="collapse' + i + '" class="panel-collapse collapse"><ul class="list-group">';
		$.each(pripady, function (i, prip) {
			out += '<li class="list-group-item ' + status(prip.stav) + '">' + prip.case + timeFrame(prip.podani, prip.rozhodnuti) + '</li>';
		});
		out += '</ul></div></div>';
	});
	out += '</div>';
	$('.grafika').html(out);
	//$('.score').hover(function() {
	//	$('<div class="ttip"><span class="win">vyhrál</span>/<span class="lost">prohrál</span>/<span class="gray">celkem</span></div>')
	//	.appendTo('.grafika')
	//}, function() {
	//	$('.ttip').remove()
	//}).mousemove(function(e) {
	//    var mousex = e.pageX + 20; 
	//    var mousey = e.pageY + 10;
	//    $('.ttip').css({ top: mousey, left: mousex })
	//});
});